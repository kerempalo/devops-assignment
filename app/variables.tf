# ---------------------
# AWS Provider
# ---------------------
variable "aws_profile" {
  type = string
  default = "default"
}

variable "aws_region" {
  type = string
  default = "ap-southeast-1"
}

# ---------------------
# API GATEWAY
# ---------------------
variable "stage_name" {
  description = "API Gateway Stage, default to `test`."
  type        = string
  default     = "test"
}

variable "api_domain_name" {
  type        = string
  description = "DNS domain in the AWS account which you own or is linked via NS records to a DNS zone you own, default to `helloworld.myapp.earth`."
  default     = "helloworld.myapp.earth"
}

variable "certificate_arn" {
  description = "API Gateway Regional certificate arn."
  type        = string
  default     = "arn:aws:acm:ap-southeast-1:847787987541:certificate/1601ec4c-26ea-4c63-8566-294ff764a611"
}

# ---------------------
# LAMBDA FUNCTIONS
# ---------------------
variable "runtime" {
  type    = string
  default = "nodejs18.x"
}

variable "architectures" {
  type    = list(string)
  default = ["x86_64"]
}

variable "package_type" {
  type    = string
  default = "Zip"
}

variable "handler" {
  type    = string
  default = "handler.hello"
}

variable "memory_size" {
  type    = number
  default = 128
}

# ---------------------
# GENERAL USE
# ---------------------
variable "name" {
  description = "The string that is used in the `name` filed or equivalent in most resources."
  type        = string
  default     = "helloworld-localstack"
}

variable "tags" {
  type    = map(string)
  default = {}
}