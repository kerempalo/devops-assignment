output "apgw_invoke_url" {
  value = aws_api_gateway_deployment.MyDemoDeployment.invoke_url
  description = "The URL to invoke the API Gateway"
}
